import { Component } from 'react';

const routesConfig = [
  {
    path: '/',
    Component: NamedNodeMap,
    exact: true,
  },
  {
    path: '/user',
    Component: NamedNodeMap,
    exact: true,
  },
];

export default routesConfig;
