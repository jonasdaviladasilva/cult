import React, { Component } from "react";

class User extends Component {
  render() {
    const list = [
      {
        name: "Jonas",
        email: "jonas.silva@gmail.com"
      },
      {
        name: "João",
        email: "joao@gmail.com"
      }
    ];
    return (
      <div>
        <table border="1">
          <tr>
            <th> Name </th> <th> Email </th>{" "}
            {list.map(item => (
              <tr>
                <td> {item.name} </td> <td> {item.email} </td>{" "}
              </tr>
            ))}{" "}
          </tr>
        </table>{" "}
      </div>
    );
  }
}

export default User;
